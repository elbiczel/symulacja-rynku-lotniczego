#!/usr/bin/env python
from Flight import Flight
from Airline import Airline
from GreedyStrategy import GreedyStrategy
from EMSRLimitsStrategy import EMSRLimitsStrategy
from Simulation import Simulation
from Params import createAirlines
from utils import *
from MarketingStrategy import *

import pkg_resources
pkg_resources.require("matplotlib")
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

airlines = createAirlines(EMSRLimitsStrategy(), EMSRLimitsStrategy())

simulation = Simulation(airlines)
simulation.simulate(leaderMarketingStrategy = MarketingStrategy(1, 1, 0), # leader ma taka sama strategie przez caly czas
                    followerFirstMarketingStrategy = MarketingStrategy(1, 1, 0), # follower zaczyna z jakas strategia
                    followerSecondMarketingStrategy = MarketingStrategy(1, 1, 1)) # a po ustalonej liczbie dni ustala nowa strategie

for airline in airlines:
  print "Airline %s revenue = %d" % (airline.name, airline.revenue)

#for (_,flight) in simulation.flights:
#  print "Tickets sold for flight %d:  %3d business, %3d leisure" % \
#        (flight.flightNumber, flight.ticketsSoldBusiness, flight.ticketsSoldLeisure)

flightsA=airlines[0].flights
flightsB=airlines[1].flights

plt.figure(1, figsize=(6,6))
plt.plot(range(0,90), list(partial_sums(flightsA[0].ticketsSoldBusiness)), label="A: Flight 1 business")
plt.plot(range(0,90), list(partial_sums(flightsA[0].ticketsSoldLeisure)), label="A: Flight 1 leisure")
plt.plot(range(0,90), list(partial_sums(flightsA[1].ticketsSoldBusiness)), label="A: Flight 2 business")
plt.plot(range(0,90), list(partial_sums(flightsA[1].ticketsSoldLeisure)), label="A: Flight 2 leisure")
plt.legend(loc=2)
plt.savefig('plot_A.png')
plt.clf()

plt.figure(1, figsize=(6,6))
plt.plot(range(0,90), list(partial_sums(flightsB[0].ticketsSoldBusiness)), label="B: Flight 1 business")
plt.plot(range(0,90), list(partial_sums(flightsB[0].ticketsSoldLeisure)), label="B: Flight 1 leisure")
plt.plot(range(0,90), list(partial_sums(flightsB[1].ticketsSoldBusiness)), label="B: Flight 2 business")
plt.plot(range(0,90), list(partial_sums(flightsB[1].ticketsSoldLeisure)), label="B: Flight 2 leisure")
plt.legend(loc=2)
plt.savefig('plot_B.png')


# TODO Wypisanie bardziej szczegolowych wynikow, zrobienie jakichs wykresow itp.

