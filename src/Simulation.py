import Params
import numpy as np
import random
from Passenger import Passenger

def poisson(mean):
    """
    Generates a random number from the Poisson distribution.

    @param mean:    a lambda parameter from the Poisson distribution    
    @type mean: number
    @return:    a random number
    """
    assert mean > 0, "ujemna wartosc w poissonie"

    suma = 0
    pick = random.random()

    for k in xrange(0, 500): # powyzej tego problemy z dokladnoscia
        val = np.exp(-mean)
        assert val > 0, "za duzy 'mean' w poissonie"

        for i in xrange(k):
            val *= mean
            val /= i+1

        suma += val
        if suma >= pick:
            return k
    return k + 1

class Simulation(object):
  def __init__(self, airlines):
    self.airlines = airlines
    self.flights = [(airline, flight) for airline in airlines for flight in airline.flights]
    
    leisureCount = int(random.normalvariate(Params.DemandMeanLeisure, Params.DemandSigmaLeisure))
    businessCount = int(random.normalvariate(Params.DemandMeanBusiness, Params.DemandSigmaBusiness))
    self.businessPassengers = [Passenger(isBusiness=True) for i in xrange(businessCount)]
    self.leisurePassengers = [Passenger(isBusiness=False) for i in xrange(leisureCount)]
    # TODO: TUTAJ GENEROWANIE SIECI SPOLECZNEJ

  def todaysDemand(self, day, isBusiness):
    vt = np.exp(
      -(Params.ShapeBusiness if isBusiness else Params.ShapeLeisure) * (Params.TicketingTime - day))
    vt = (Params.intensityBusiness if isBusiness else Params.intensityLeisure) * vt
    return poisson(vt)

  def simulate(self, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy):
    resultLogs = {"ticketsSold": []}

    for passenger in self.businessPassengers:
      passenger.resetForNewSimulation()
    for passenger in self.leisurePassengers:
      passenger.resetForNewSimulation()

    leader, follower = self.airlines
    leader.initialize(leaderMarketingStrategy)
    follower.initialize(followerFirstMarketingStrategy)
    
    for cycle in xrange(0, Params.CyclesNumber):
      for airline in self.airlines:
        airline.newCycle()

      for day in xrange(0, Params.TicketingTime):
      
        if day + cycle * Params.TicketingTime == Params.FollowerMarketingStrategyDelay:
          follower.setMarketingStrategy(followerSecondMarketingStrategy)

        todaysBusinessDemand = self.todaysDemand(day, True)
        todaysLeisureDemand = self.todaysDemand(day, False)

        todaysPassengers = random.sample(self.businessPassengers, todaysBusinessDemand)
        todaysPassengers.extend(random.sample(self.leisurePassengers, todaysLeisureDemand))
        random.shuffle(todaysPassengers)

        # Pasazer wybiera lot i kupuje bilet.
        for passenger in todaysPassengers:
          flightsWithUtility = sorted([(-passenger.utilityFunction(offer), airline, offer)
                                for airline in self.airlines
                                for offer in airline.getOffers(passenger.isBusiness)])
          for (_, airline, offer) in flightsWithUtility:
            if airline.buyTicket(offer, passenger.isBusiness, day):
              # Kupil bilet i polecial, uaktualniamy satisfaction:
              passenger.updateSatisfaction(airline, offer["quality"])
              # zapisujemy to w logach
              resultLogs["ticketsSold"].append((airline.name, offer["flight"].flightNumber, cycle, day, passenger.isBusiness, offer["fare"], offer["quality"]))
              break
              
      for airline in self.airlines:
        airline.endCycle()
    
    resultLogs["revenue"] = [airline.revenue for airline in self.airlines]
    return resultLogs
