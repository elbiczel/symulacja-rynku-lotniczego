from Flight import Flight
from Airline import Airline
from MarketingStrategy import *
import json

CyclesNumber = 5
TicketingTime = 90
FollowerMarketingStrategyDelay = CyclesNumber * TicketingTime / 2
SatisfactionInfectionTrigger = 1.1

DemandMeanBusiness = 95
DemandMeanLeisure = 450
ShapeBusiness = 0.01
ShapeLeisure =  0.035
intensityBusiness = 5
intensityLeisure = 10
DemandSigmaBusiness = 9.75
DemandSigmaLeisure = 21

airline1Capacity = 120
airline2Capacity = 150

numBusinessPassengers = 40
numLeisurePassengers = 40

NeutralStrategy = MarketingStrategy(0, 0, 0)

def loadJSONParams(filePath):
	json_data=open(filePath)
	data = json.load(json_data)
	json_data.close()
	CyclesNumber = float(data["CyclesNumber"])
	TicketingTime = float(data["TicketingTime"])
	SatisfactionInfectionTrigger = float(data["SatisfactionInfectionTrigger"])
	DemandMeanBusiness = float(data["DemandMeanBusiness"])
	DemandMeanLeisure = float(data["DemandMeanLeisure"])
	ShapeBusiness = float(data["ShapeBusiness"])
	ShapeLeisure = float(data["ShapeLeisure"])
	intensityBusiness = float(data["intensityBusiness"])
	intensityLeisure = float(data["intensityLeisure"])
	DemandSigmaBusiness = float(data["DemandSigmaBusiness"])
	DemandSigmaLeisure = float(data["DemandSigmaLeisure"])
	airline1Capacity = float(data["airline1Capacity"])
	airline2Capacity = float(data["airline2Capacity"])
	numBusinessPassengers = float(data["numBusinessPassengers"])
	numLeisurePassengers = float(data["numLeisurePassengers"])

	FollowerMarketingStrategyDelay = CyclesNumber * TicketingTime / 2
	return data

def createAirlines(strategyA, strategyB):
  flightsA = [Flight(101,  7*60, 12*60, 150, 60, airline1Capacity),
              Flight(102, 10*60, 15*60, 150, 60, airline1Capacity)]
  flightsB = [Flight(201,  8*60, 13*60, 170, 60, airline2Capacity),
              Flight(202,  9*60, 14*60, 150, 60, airline2Capacity)]

  airlines = [Airline("A", flightsA, strategyA),
              Airline("B", flightsB, strategyB)]

  return airlines

