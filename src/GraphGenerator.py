import random


class GraphGenerator(object):
  def __init__(self, avgDegree, gamma, number_of_nodes):
    self.avgDegree = avgDegree
    self.number_of_nodes = number_of_nodes
    self.gamma = gamma
    self.factor = (avgDegree * number_of_nodes) / (gamma - 1.0)
    self.degree_distribution = [0]
    nodes_left, degree = 0, 0

    while nodes_left < number_of_nodes:
      degree += 1
      v = 1 if random.random() < 0.5 else 0
      power_function_result = self.factor * (degree ** (-gamma))
      if power_function_result >= 1.0:
        v = int(power_function_result)
      if v > nodes_left:
        v = nodes_left
      self.degree_distribution.append(v)
      nodes_left -= v
    self.graph = {}
    self.graph.fromkeys([i for i in xrange(self.number_of_nodes)], set([]))

  def generate(self, initial_number_of_vertices, edges_added_per_iteration):
    degrees = [0 for _ in xrange(initial_number_of_vertices)]
    sum_of_degrees = 0
    for id in xrange(initial_number_of_vertices):
      other = random.randint(0, initial_number_of_vertices)
      if other == id:
        u = (u - 1) if u > 0 else (u + 1)
        self.graph[id].add(other)
        self.graph[other].add(id)
        degrees[id] += 1
        degrees[other] += 1
        sum_of_degrees += 2

    for id in xrange(initial_number_of_vertices, self.number_of_nodes):
      added = 0
      while added < edges_added_per_iteration:
        sum = random.randint(0, sum_of_degrees)
        other = 0
        while sum >= 0:
          sum -= degrees[other]
          other += 1
        other -= 1
        self.graph[id].add(other)
        self.graph[other].add(id)
        added += 1
        degrees[other] += 1
        sum_of_degrees += 1
      degrees.append(edges_added_per_iteration)
      sum_of_degrees += edges_added_per_iteration
