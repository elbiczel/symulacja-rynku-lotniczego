import numpy as np

def NewsBoyCapacity(Dist, Underage, Overrage):
    """ ???
    Quantile[Dist, Underage/(Underage + Overrage)]
    jaka funkcja do liczenie kwantyli?!
    """
    q = Underage/(Underage + Overrage)
    return np.percentile(Dist, q * 100)
    

def EMSRLimits(j, DistList, fares, Cap):
    if j == 0:
        return 0
    capacities = [NewsBoyCapacity(DistList[i], fares[i] - fares[j], fares[j]) for i in xrange(0,j)]
    result = sum(capacities) # to chyba znaczy ze dla wszystkich mniejszych ? {i, 1, j - 1})
   
    return min(result, Cap)


def EMSRLimitsList(j, DistList, fares, Cap):
    """ ???
    Table[EMSRLimits[i, DistList, fares, Cap], {i, 1, j}]
    """
    return [EMSRLimits(i, DistList, fares, Cap) for i in xrange(0, j)]

def demand (mean, DistList):
    """ ???
    demand := Round[Map[Mean, DistList]];
    co to jest Mean?
    """
    return
    
    
def revenue(m, n, demand, klevels, fares):
    """
    Revenue[1,n_,demand_,klevels_,fares_]:=
        fares[[1]]*n/;0<=n&&n<demand[[1]]
    """
    if m == 1 and demand[1] > n and n >= 0:
        return fares[1]*n
    """
    Revenue[1,n_,demand_,klevels_,fares_]:=
        fares[[1]]*demand[[1]]/;demand[[1]]<=n
    """
    if demand[1] <=n:
        return fares[1]*demand[1]
    """
    Revenue[m_,n_,demand_,klevels_,fares_]:=
        Revenue[m-1,n,demand,klevels,fares]/;0<=n&&n<=klevels[[m]]
    """
    if n >= 0 and klevels[m] >= n:
        return revenue(m-1, n, demand, klevels, fares)
    """
    Revenue[m_,n_,demand_,klevels_,fares_]:=
        (n-klevels[[m]])*fares[[m]]+ Revenue[m-1,klevels[[m]],demand,klevels,fares]/;
            klevels[[m]]<n&&demand[[m]]>n-klevels[[m]]
    """
    if klevels[m] < n and demand[m] > n - klevels[m]:
        return (n- klevels[m]) * fares[m] + revenue(m-1, klevels[m], demand, klevels, fares)
    
    """
    Revenue[m_,n_,demand_,klevels_,fares_]:=
        demand[[m]]*fares[[m]]+Revenue[m-1,n-demand[[m]],demand,klevels, fares]/;
            klevels[[m]]<n&&demand[[m]]<=n-klevels[[m]]
    """
    if klevels[m] < n and demand[m] <= n - klevels[m]:
        return klevels[m] * fares[m] + revenue(m-1, n-demand[m], demand, klevels, fares)


def init():
    d11 = np.random.binomial(98, 0.3284)
    d12 = np.random.binomial(250, 0.1857)
    d13 = np.random.binomial(365, 0.2539)
    d14 = np.random.binomial(1500, 0.1875)
    d15 = np.random.binomial(1835, 0.1573)
    
    DistList = [d11, d12, d13, d14, d14] 
    """ ???
    demand = Round[Map[Mean, DistList]];
    [ [.............], 
      [.............],
      [.............] ]
      
      [sr1, sr2, sr3]
    co to jest Mean?
    """
    fares = [1049.0, 675.0, 534.0, 499.0, 350.0]
    Cap = 119
    klevels = EMSRLimitsList(5, DistList, fares, Cap)
    print klevels
    """ ???
    Table[Revenue[i, 119, demand, klevels, fares], {i, 1, 5}]
    LoadList := 
         Module[{a0, a1, a2, a3, a4, a5}, 
            a0 = Table[Revenue[i, 119, demand, klevels, fares], {i, 1, 5}]; 
            a1 = a0[[1]]/fares[[1]]; a2 = (a0[[2]] - a0[[1]])/fares[[2]]; 
            a3 = (a0[[3]] - a0[[2]])/fares[[3]]; 
            a4 = (a0[[4]] - a0[[3]])/fares[[4]]; 
            a5 = (a0[[5]] - a0[[4]])/fares[[5]]; 
            Map[Round, {a1, a2, a3, a4, a5}]] // N
    LoadFactor := Total[LoadList]/Cap
    """
    
init()
    
