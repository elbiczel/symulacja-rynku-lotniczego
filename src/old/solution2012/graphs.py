""" 
Classes and helpers for creating charts. They allow to show results of simulations.
"""

# Rozmiar
SIZE_X = 1200
SIZE_Y = 800

class PieGraph(object):
    """
    Class that represents a pie chart.
    """
    def __init__(self, title, legend):
        """ 
        Initializes a chart with given parameters.

        @param title:	the title of chart
        @type title:	string
        @param legend:	the description of data
        @type legend:	string
        """
        self.title = title
        self.legend = legend

        self.data = [ 0 for i in xrange(len(legend)) ]
        self.sets = 0.0 # liczba prob

    def add(self, *args):
        """
        Adds new data to the chart.

        @param args:	data
        @type args: 	pointer
        """
        assert len(args) == len(self.data)

        for i, arg in enumerate(args):
            self.data[i] += arg

    def new_set(self):
        """
        Increases the number of sets.
        """
        self.sets += 1

    def to_html(self, chart_num):
        """
        Generates the php code which creates the chart.

        @param chart_num:	the ordinal number of chart
        @type chart_num:    number
        @return:	        php code
        """
        point = '$dataSet->addPoint(new Point("{legend}", {value}));'

        html = ['$chart = new PieChart({x}, {y});'.format(
                    x=SIZE_X,
                    y=SIZE_Y
                )]
        html.append('$dataSet = new XYDataSet();')

        for i in xrange(len(self.data)): # punkty
            html.append(point.format(
                            legend=self.legend[i],
                            value=self.data[i]/self.sets
                        ))

        html.append('$chart->setDataSet($dataSet);')
        html.append('$chart->setTitle("{0}");'.format(self.title))
        html.append('$chart->getPlot()->setGraphCaptionRatio(0.82);')
        html.append('$chart->render("grafy/g{0}.png");'.format(chart_num))

        return '\n'.join(html)


class LineGraph(object):
    """
    Class that represents line plot.
    """
    def __init__(self, title, legend, increase=False):
        """ 
        Initializes a plot with given parameters.

        @param title:	the title of plot
        @type title:	string
        @param legend:	the description of data
        @type legend:	string
        @param increase:	true if the data should be summed
        @type increase:	bool
        """
        self.title = title
        self.legend = legend
        self.increase = increase # flaga czy sumowac dane (np. z dziennego popytu otrzymamy wtedy laczny popyt)

        self.data = [ [0] for i in xrange(len(legend)) ] # dwuwymiarowa tablica, kazdy wiersz zawiera dane dla jednej linii grafu
        self.sets = 0.0

    def add(self, *args):
        """
        Adds new data to the plot.

        @param args:	data
        @type args: 	pointer
        """
        assert len(args) == len(self.data)

        for i, arg in enumerate(args):
            if len(self.data[i]) <= self.it:
                self.data[i].append(arg)
            else:
                self.data[i][self.it] += arg

        self.it += 1

    def new_set(self):
        """
        Increases the number of sets.
        """
        self.it = 1
        self.sets += 1

    def to_html(self, chart_num):
        """
        Generates html code.

        @param chart_num:	the ordinal number of chart
        @type chart_num:	number
        """
        xyset = '$serie{nr} = new XYDataSet();'
        point = '$serie{nr}->addPoint(new Point({x}, {y}));'
        add_data = '$dataSet->addSerie("{legend}", $serie{nr});';

        html = ['$chart = new LineChart({x}, {y});'.format(
                    x=SIZE_X,
                    y=SIZE_Y
                )]
        html.append('$dataSet = new XYSeriesDataSet();')

        for i in xrange(len(self.data)): # series
            html.append(xyset.format(nr=i))

            for j in xrange(len(self.data[0])): # points
                if j > 0 and self.increase:
                    self.data[i][j] += self.data[i][j-1] # dodaj poprzednia wartosc

                html.append(point.format(
                                nr=i,
                                x=j,
                                y=self.data[i][j]/self.sets
                            ))

            html.append(add_data.format(
                            legend=self.legend[i],
                            nr=i
                        ))

        html.append('$chart->setDataSet($dataSet);')
        html.append('$chart->setTitle("{0}");'.format(self.title))
        html.append('$chart->getPlot()->setGraphCaptionRatio(0.82);')
        html.append('$chart->render("grafy/g{0}.png");'.format(chart_num))

        return '\n'.join(html)


def create_html(filename, *graphs):
    """
    Creates html file which shows charts - one for each simulation.

    @param graphs:	charts to be shown
    @type graphs:	pointer
    @return:	html file
    """
    html_body = """
<?php
    include "libchart/libchart/classes/libchart.php";
    {php_graphs}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Symulacja</title>
</head>
<body>
<a href=lot.php>Powrot</a>
<br />
<br />
{images}
<br />
<br />
<a href=lot.php>Powrot</a>
</body>
</html>
"""
    image = '<img alt="Line chart" src="grafy/g{num}.png" style="border: 1px solid gray;"/>'

    g_code = []
    i_code = []

    try:
        f = open('cnt_png', 'r')
        offset = int(f.read())
        f.close()
    except:
        offset = 1

    for i, g in enumerate(graphs):
        g_code.append(g.to_html(offset))
        i_code.append(image.format(num=offset))
        offset += 1
    
    with open('cnt_png', 'w') as f:
        f.write(str(offset))

    with open(filename, "w") as f:
        f.write(html_body.format(
                    php_graphs='\n'.join(g_code),
                    images='<br />\n'.join(i_code)
                ))
