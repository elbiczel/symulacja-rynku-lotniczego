
"""
    Generates php file with a form that allows to change model's parameters.
"""

tab = [
    "b-fare",
    "b-duration",
    "b-arrive_early",
    "b-arrive_late",
    "b-depart_early",
    "b-depart_late",
    "b-lower_window",
    "b-upper_window",
    "b-intensity",
    "b-shape",
    "b-demand_mean",
    "b-demand_sigma"
]

td = '<td><input type="text" name="{t}-{val}" value="<?php print $data->{{"{t}-{val}"}}; ?>" /></td>'

d = ["""
<?php
        $json = file_get_contents("json.data");
        $data = json_decode($json);
?>
<html>
<body>

<form method="post" action="z.php">
<table>
<tr>
<th></th>
<th>Business</th>
<th>Leisure</th>
</tr>
"""]

for key in tab:
    val = key.split('-')[1]
    d.append('<tr>')
    d.append('<td>{0}</td>'.format(val))
    d.append(td.format(t="b", val=val))
    d.append(td.format(t="l", val=val))
    d.append('</tr>')

d.append("""
</table>
<input type="submit" value="symuluj">
</form>

</body>
</html>
""")

with open('lot.php', 'w') as f:
    f.write('\n'.join(d))
