"""
General helpers.
"""

import random, math, datetime, json



params = None # parametry symulacji

def load_params(json_file):
    """ 
    Loads parameters from the given json file.

    @param json_file: the name of file where the parameters are
    @type json_file: string
    """
    global params
    params = {
        AgentType.Business: {},
        AgentType.Leisure: {}
    }

    tab = json.loads(open(json_file, "r").read())

    for key in tab:
        org_key = key.split('-')[1]

        if key.startswith('b-'):
            params[AgentType.Business][org_key] = float(tab[key])
        elif key.startswith('l-'):
            params[AgentType.Leisure][org_key] = float(tab[key])
        else:
            assert False, "Zly typ"


class AgentType:
    """
    Class that represents possible types of agents.
    """
    Business = 1
    Leisure = 2

class Agent(object):
    """ 
    Class that represents a passenger.
    """
    def __init__(self, ttype):
        """
        Initializes an agent.

        His parameters are drawn from specified distributions with parameters given in a specified table.

        @param ttype: a type of agent
        @type ttype: AgentType
        """
        self.ttype = ttype

        self.params = {}
        for key, value in params[ttype].items():
            if value >= 0: # te parametry po prostu przepisujemy
                self.params[key] = value
            else:          # a te losujemy z rozkladu normalego obcietego do zera
                p_val = 0
                while p_val >= 0:
                    p_val = random.normalvariate(
                                mu = value,
                                sigma = math.sqrt(math.fabs(value))
                            )
                self.params[key] = p_val

        # parametr czasu idealnego odlotu
        value = random.normalvariate(mu = 8, sigma = 1.5)
        hour = int(value)
        mins = int((value - hour) * 60)
        self.params['departure_time'] = datetime.time(hour, mins)

    def pick_flight(self, flights):
        """
        Chooses the best offer from the list of all possible flights by comparing the values of utility function.

        @param flights: a set of all possible flights
        @type flights: a list of Flights
        """
        best = (None, float('-inf')) # nr lotu i jego uzytecznosc

        for i, flight in enumerate(flights):
            if flight.not_full(self.ttype): # trzeba sprawdzic czy sa jeszcze miejsca
                value = self.utility_function(flight)
                if value > best[1]:
                    best = (i, value)

        if best[0] is not None:
            flights[best[0]].book(self.ttype)

    def utility_function(self, flight):
        """
        Calculates an utility function for a given flight.

        @param flight: 	a single flight which utility value one would like to calculate
        @type flight:	Flight
        @return:	the value of utility function
        """
        result = self.params['fare'] * math.log(flight.price(self.ttype))
        result += self.params['duration'] * flight.length
        
        time_diff = (self._to_float(self.params['departure_time']) -
                     self._to_float(flight.departure))

        # czesc czasowa
        if time_diff > 0:
            window_diff = time_diff - self.params['lower_window']
            if window_diff > 0: # lot jest za wczesnie
                result += self.params['depart_early'] * (window_diff + 1)
        if time_diff < 0:
            window_diff = (-time_diff) - self.params['upper_window']
            if window_diff > 0: # lot jest za pozno
                result += self.params['depart_late'] * (window_diff + 1)
        return result

    def _to_float(self, time):
        """
        Creates a double-object from a datatime.time-object.

        @param time:	a datetime.time-object
        @type time: 	datetime.time
        @return:	a float-object
        """
        return time.hour + time.minute/60.0

def __prepare_type(ttype):
    """
    Creates agents of a given type. The number of them is drawn from a distribution which parameters correspond to the given type.

    @param ttype: type of agents which should be created
    @type ttype: AgentType
    @return: set of Agents
    """
    count = int( random.normalvariate(
                     params[ttype]['demand_mean'],
                     params[ttype]['demand_sigma']
                ))
    return [ Agent(ttype) for i in xrange(count) ]

def prepare_agents():
    """
    Creates agents of both leisure and business classes.

    @return: two sets of Agents
    """
    bagents = __prepare_type(AgentType.Business)
    lagents = __prepare_type(AgentType.Leisure)
    return bagents, lagents

##
## Flight helpers
##

class Flight(object):
    """
    Class that represents a flight.
    """
    def __init__(self, stats, seats, owner):
        """
        Creates a flight with given parameters.

        @param stats:	information about the flight (departure time, arrival time, length of flight, business-fare, leisure-fare, protection)
        @type stats:	a list
        @param seats:	the number of seats in the offer
        @type seats:	number
        @param owner:	the name of an airline company
        @type owner:	string
        """
        self.stats = stats
        self.seats = seats
        self.taken = 0
        self.profit = 0
        self.owner = owner

    def __getattr__(self, attr):
        """
        Enables to get certain attribute of a flight.

        @param attr:	the name of the parameter
        @type attr:	string
        @return:	the value of the parameter
        """
        if attr in self.stats:
            return self.stats[attr]
        else:
            raise AttributeError(attr)

    def book(self, ttype):
        """
        Books a seat for an agent.

        @param ttype:	a type of a reserved seat
        @type ttype:	AgentType
        """
        assert self.taken < self.seats, "Over-booking"
        self.taken += 1
        self.profit += self.price(ttype)

    def not_full(self, ttype):
        """
        Checks whether there are still available seats. Remembers the protection level.

        @param ttype:	a type of seat that somebody wants to reserve
        @type ttype:	AgentType
        @return:	true if there is an available seat, false in the other case
        """
        if ttype == AgentType.Business:
            return self.seats - self.taken > 0
        elif ttype == AgentType.Leisure:
            return self.seats - self.taken > self.protection
        else:
            assert False, "Typ agenta"

    def price(self, ttype):
        """
        Returns the price of a ticket depending on the type of agent.

        @param ttype:	the type of agent
        @type ttype:	AgentType
        @return:	the price of ticket
        """
        if ttype == AgentType.Business:
            return self.stats['bfare']
        elif ttype == AgentType.Leisure:
            return self.stats['lfare']
        else:
            assert False, "Typ agenta"

def prepare_flights():
    """
    Creates flights with parameters described in the assignment.

    @return: a set of flights
    """
    flights = [
        Flight( stats={
                'departure': datetime.time(7),
                'arrival': datetime.time(12),
                'length': 5.0,
                'bfare': 150,
                'lfare': 60,
                'protection': 28
            },
            seats=120,
            owner="A"
        ),
        Flight( stats={
                'departure': datetime.time(10),
                'arrival': datetime.time(15),
                'length': 5.0,
                'bfare': 150,
                'lfare': 60,
                'protection': 28
            },
            seats=120,
            owner="A"
        ),
        Flight( stats={
                'departure': datetime.time(8),
                'arrival': datetime.time(13),
                'length': 5.0,
                'bfare': 170,
                'lfare': 60,
                'protection': 28
            },
            seats=150,
            owner="B"
        ),
        Flight( stats={
                'departure': datetime.time(9),
                'arrival': datetime.time(14),
                'length': 5.0,
                'bfare': 150,
                'lfare': 60,
                'protection': 28
            },
            seats=150,
            owner="B"
        )
    ]
    return flights

##
## Math helpers
##

def poisson(mean):
    """
    Generates a random number from the Poisson distribution.

    @param mean:	a lambda parameter from the Poisson distribution	
    @type mean:	number
    @return:	a random number
    """
    assert mean > 0, "ujemna wartosc w poissonie"

    suma = 0
    pick = random.random()

    for k in xrange(0, 500): # powyzej tego problemy z dokladnoscia
        val = math.exp(-mean)
        assert val > 0, "za duzy 'mean' w poissonie"

        for i in xrange(k):
            val *= mean
            val /= i+1

        suma += val
        if suma >= pick:
            return k
    return k + 1

def todays_demand(total_days, act_day, ttype):
    """
    Return the value from a demand-curve for the given day and given type of passenger.

    @param total_days:	a length of simulation
    @type total_days:	number
    @param act_day:	the number of days which have passed
    @type act_day:	number
    @param ttype:	a type of passenger
    @type ttype:	AgentType
    @return:	the value from a demand-curve
    """
    vt = math.exp(-params[ttype]['shape'] * (total_days - act_day))
    vt = params[ttype]['intensity'] * vt

    return poisson(vt)
