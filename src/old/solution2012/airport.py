"""
Runs the simulations.
"""

#!/usr/lib/env python
# -*- coding: utf-8
import sys, random

from helpers import *
from graphs import LineGraph, PieGraph, create_html


def run(json_file, html_name):
    """
    Runs 100 simulations and creates the output file with the results.

    @type json_file: 	string
    @param json_file: 	file in which you can find all the parameters for the simulation
    @type html_name:	string
    @param html_name: 	the name for the html file which will be generated
    """

    DAYS = 90

    # grafy
    popyt = LineGraph('ogolny popyt', ['business', 'leisure'], increase=True)
    loty = LineGraph('sprzedaz biletow', ['lot A1', 'lot A2', 'lot B1', 'lot B2'], increase=False)
    rynek = PieGraph('udzial w rynku', ['lot A1', 'lot A2', 'lot B1', 'lot B2'])

    load_params(json_file)

    for k in xrange(100): # liczba symulacji

        popyt.new_set()
        loty.new_set()
        rynek.new_set()

        ba_agents, la_agents = prepare_agents() # zawsze bierzemy nowych agentow
        flights = prepare_flights()             # i resetujemy loty

        for i in xrange(1, DAYS + 1):
            # sprawdzamy demand
            bb = todays_demand(DAYS, i, AgentType.Business)
            lb = todays_demand(DAYS, i, AgentType.Leisure)

            # losujemy podana liczbe agentow
            todays_agents = []
            todays_agents.extend( random.sample(ba_agents, bb) )
            todays_agents.extend( random.sample(la_agents, lb) )
            # i wykonujemy shuffle dla symulacji roznej kolejnosci zgloszen
            random.shuffle(todays_agents)

            for agent in todays_agents:
                agent.pick_flight(flights)

            # dodajemy odpowiednie dane do grafow
            popyt.add(bb, lb)
            loty.add(*[ f.taken for f in flights ])

        # zyski po pojedynczej symulacji
        rynek.add(*[ f.profit for f in flights ])

    # po petli
    create_html(html_name, popyt, loty, rynek)


if __name__=="__main__":
    assert len(sys.argv) == 3
    run(
        json_file=sys.argv[1], # plik z danymi
        html_name=sys.argv[2]  # nazwa strony php do utworzenia
    )
