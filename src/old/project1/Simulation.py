import Params
import numpy as np
import random
from Passenger import Passenger

def poisson(mean):
    """
    Generates a random number from the Poisson distribution.

    @param mean:    a lambda parameter from the Poisson distribution    
    @type mean: number
    @return:    a random number
    """
    assert mean > 0, "ujemna wartosc w poissonie"

    suma = 0
    pick = random.random()

    for k in xrange(0, 500): # powyzej tego problemy z dokladnoscia
        val = np.exp(-mean)
        assert val > 0, "za duzy 'mean' w poissonie"

        for i in xrange(k):
            val *= mean
            val /= i+1

        suma += val
        if suma >= pick:
            return k
    return k + 1

class Simulation(object):
  def __init__(self, airlines):
    self.airlines = airlines
    self.flights = [(airline, flight) for airline in airlines for flight in airline.flights]

  def todaysDemand(self, day, isBusiness):
    vt = np.exp(
      -(Params.ShapeBusiness if isBusiness else Params.ShapeLeisure) * (Params.TicketingTime - day))
    vt = (Params.intensityBusiness if isBusiness else Params.intensityLeisure) * vt
    return poisson(vt)

  def simulate(self):
    leisureCount = int(random.normalvariate(Params.DemandMeanLeisure, Params.DemandSigmaLeisure))
    businessCount = int(random.normalvariate(Params.DemandMeanBusiness, Params.DemandSigmaBusiness))
    businessPassengers = [Passenger(isBusiness=True) for i in xrange(businessCount)]
    leisurePassengers = [Passenger(isBusiness=False) for i in xrange(leisureCount)]

    for day in xrange(0, Params.TicketingTime):
#      print "dzien %d" % day
      todaysBusinessDemand = self.todaysDemand(day, True)
      todaysLeisureDemand = self.todaysDemand(day, False)

      todaysPassengers = random.sample(businessPassengers, todaysBusinessDemand)
      todaysPassengers.extend(random.sample(leisurePassengers, todaysLeisureDemand))
      random.shuffle(todaysPassengers)

      for passenger in todaysPassengers:
        flightsWithUtility = sorted([(-passenger.utilityFunction(flight), airline, flight)
                              for (airline, flight) in self.flights])
        for (_, airline, flight) in flightsWithUtility:
          if airline.buyTicket(flight, passenger.isBusiness, day):
            break
