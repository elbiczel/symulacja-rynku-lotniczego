from Strategy import Strategy

class GreedyStrategy(Strategy):
  def canBuy(self, flight, isBusiness):
    return not flight.isFull()
