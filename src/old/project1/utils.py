def partial_sums(iterable):
    total = 0
    for i in iterable:
        total += i
        yield total
