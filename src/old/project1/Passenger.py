import random
import math
import datetime

class Passenger(object):

  def __init__(self, isBusiness):
    self.isBusiness = isBusiness
    self.arriveEarlyWeight = -0.161 if isBusiness else -0.092
    self.arriveLateWeight = -0.471 if isBusiness else -0.449

    self.departEarlyWeight = -0.803 if isBusiness else -0.364
    self.departLateWeight = -0.522 if isBusiness else -0.094
    self.durationWeight = -1.227 if isBusiness else -0.54
    self.fare = -4.216 if isBusiness else -4.549
    self.lowerWindow = 0.75 if isBusiness else 1.5
    self.upperWindow = 0.5 if isBusiness else 1.25
    self.idealDepartureTime = random.normalvariate(mu = 8, sigma = 1.5) # w godzinach



  def utilityFunction(self, flight):
    result = self.fare * math.log(flight.businessFare if self.isBusiness else flight.leisureFare)
    result += self.durationWeight * (flight.arrivalTime - flight.departureTime) / 60.0
    
    timeDiff = self.idealDepartureTime - (flight.departureTime / 60.0)
   
    if timeDiff > 0:
        windowDiff = timeDiff - self.lowerWindow
        if windowDiff > 0:
            result += self.departEarlyWeight * (windowDiff + 1)
    if timeDiff < 0:
        windowDiff = (-timeDiff) - self.upperWindow
        if windowDiff > 0:
            result += self.departLateWeight * (windowDiff + 1)
    return result



