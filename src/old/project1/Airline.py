class Airline(object):
  def __init__(self, name, flights, strategy):
    self.name = name
    self.flights = flights
    self.strategy = strategy
    self.revenue = 0

  def canBuy(self, flight, isBusiness):
    return self.strategy.canBuy(flight, isBusiness)
    
  def buyTicket(self, flight, isBusiness,day):
    if isBusiness:
      flight.wantedToBuyBusiness += 1
    else:
      flight.wantedToBuyLeisure += 1
    if self.canBuy(flight, isBusiness):
      self.revenue += flight.businessFare if isBusiness else flight.leisureFare
      flight.reserveTicket(isBusiness, day)
      return True
    else:
      return False
