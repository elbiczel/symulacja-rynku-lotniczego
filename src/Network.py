import random


class Network(object):
  class Vertex:
    def __init__(self, id, passenger):
      self.id = id
      self.passenger = passenger
      self.infection_types = set([])

    def infect(self, type):
      self.infection_types.add(type)

  def __init__(self, graph):
    # graph is dictionary f lists of neighborhoods
    self.graph = graph
    self.vertices = {}
    self.infected = {}
    self.infection_probability = 0.1

  def add_agent(self, id, passenger):
    self.vertices[id] = self.Vertex(id, passenger)

  def infect(self, id, type):
    self.vertices[id].infect(type)
    self.infected[id] = type

  def infect_neighbors(self, id, type):
    for neighbor in self.graph[id]:
      if random.random() < self.infection_probability:
        self.infect(neighbor, type)

  def run_campaign(self, type, number_of_infected):
    result = []
    for i in random.shuffle(range(len(self.graph.keys())))[:-number_of_infected]:
      result.append(i)
      self.infect(i, type)
    while len(self.infected) > 0:
      infected_copy = self.infected
      self.infected = {}
      for id in infected_copy:
        self.infect_neighbors(id, type)



