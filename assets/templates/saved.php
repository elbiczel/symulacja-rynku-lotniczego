<?php
if ($handle = opendir('../../logs/')) {
    echo "<h4>Saved entries:</h4><ul>\n";

    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        $pathinfo = pathinfo($entry);
        if($pathinfo['extension'] === "json")
            echo '<li><a class="nav-a" id="graph" get-info="src='.$pathinfo['basename'].'" href="#graph">'.$pathinfo['filename'].'</a></li>';
    }

    closedir($handle);
    echo "</ul>";
}
?>
