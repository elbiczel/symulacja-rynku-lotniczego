<div class="jumbotron">
	<h1>Multi-Agent Aircraft Market Simulation</h1>
	<p class="lead">Simulation of an aircraft market with two competing airlines.</p>
	<div class="accordion" id="accordion2">
	  <div class="accordion-group">
	    <div class="accordion-heading">
	      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
	        Set Simulation Parameters
	      </a>
	    </div>
	    <div id="collapseOne" class="accordion-body collapse">
	      <div class="accordion-inner">
	        <form class="form-horizontal">
			<?php
				$string = file_get_contents("../../config.default.json");
				$config = json_decode($string, true);
				foreach ($config as $name => $val){
				echo '
					<div class="control-group">
			    		<label class="control-label" for="'.$name.'">'.$name.'</label>
			    		<div class="controls">
			      			<input type="text" id="'.$name.'" value="'.$val.'">
			    		</div>
			  		</div>';
				}
			?>
			</form>
	      </div>
	    </div>
	  </div>
	</div>
	<a class="btn btn-large btn-success" id="simulation" href="#graph">Launch it!</a>

</div>