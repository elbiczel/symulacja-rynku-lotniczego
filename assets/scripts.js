$(function(){
	var wrapper = $('#content-wrapper')[0];
	$(wrapper).load('assets/templates/home.php');
	$(document).on("click", "a.nav-a", function(){
		var get=$(this).attr("get-info"),
			html = 'assets/templates/'+this.id+'.php';
		if(get)
			html += '?'+get;
		$(wrapper).load(html);
		$(".masthead .nav li").removeClass("active");
		$(".masthead .nav li a#"+this.id).parent().addClass("active");
	});

	$(document).on("click", "a#simulation", function(){
		var params={},
			progress_timer;
		$("form .control-group input").each(function(){
			if(this.id!="name")
				params[this.id] = $(this).val();
		});
		progress_timer = setInterval(function(){
			$(wrapper).load('assets/templates/state.php');
		},1000)
		$.post('assets/templates/simulation.php',{"params" : JSON.stringify(params)}, function(data) {
			if(data == "error")
				alert("error");
			else
				$(wrapper).load('assets/templates/graph.php?src='+data)
		})
		.done(function(){})
		.fail(function(){})
		.always(function(){clearInterval(progress_timer)});
	});
});
function ajax_simulation_state(){

}